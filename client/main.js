import './main.html';
import '../imports/startup/client/';

import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import ApolloApp from '../imports/startup/client/routes.jsx';

Meteor.startup(() => {
  render(<ApolloApp />, document.getElementById('app'));
});
