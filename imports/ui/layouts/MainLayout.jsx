
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Session } from 'meteor/session';

import Footer from './partials/Footer.jsx';
import MainHeader from './partials/MainHeader.jsx';
import MainSidebar from './partials/MainSidebar.jsx';
import { usingDefaultPassword } from '../../api/users/methods.js';


import { initLayout } from '../../startup/client/lib/init-layout.js';

export default class MainLayout extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if (!this.props.loggedIn) {
      this.context.router.push('/login');
    }

    if (!Session.get('usingDefaultPassword')) {
      usingDefaultPassword.call({}, (error, result) => {
        if (result) {
          Session.set('usingDefaultPassword', result.usingDefaultPassword);
          if (Session.get('usingDefaultPassword')) {
            this.context.router.replace('/password/set');
          }
        }
      });
    }
  }

  componentDidMount() {
    initLayout();
    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);
  }

  componentWillUpdate() {
    const { roles } = this.props;
    if (!this.props.loggedIn) {
      this.context.router.replace('/login');
    }

    if (roles.isBiologist) {
      this.context.router.replace('/biologist');
    } else if (roles.isBioInformatics) {
      this.context.router.replace('/bioinformatics');
    } else if (roles.isGenomics) {
      this.context.router.replace('/genomics');
    } 

    if (Session.get('usingDefaultPassword')) {
      this.context.router.replace('/password/set');
    }
  }

  render() {
    const { roles, user, content } = this.props;
    return (
      <div className="wrapper">
        <MainHeader user={user} roles={roles} />

        <MainSidebar roles={roles} />

        {content}

        <Footer />

        <div className="control-sidebar-bg" />
      </div>
    );
  }
}

MainLayout.propTypes = {
  // user: React.PropTypes.object.isRequired,
  loggedIn: React.PropTypes.string,

};

MainLayout.contextTypes = {
  router: React.PropTypes.object,
};
