import { Link } from 'react-router';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import moment from 'moment/moment';


export default class MainHeader extends Component {

  constructor(props) {
    super(props);

    let user = Meteor.user();
    this.state = {
      currentUser: user
    };

    this.logout = this.logout.bind(this);
  }

  logout() {
    Meteor.logout();
    this.context.router.push('/landing');
  }

  componentDidMount() {
  }
  
  // const profileDate = Meteor.userId.dateEmployed();
  
  render() {

  let jobTitle;
  const { user, roles, loading } = this.props;


    if(roles.isTeller) {
      jobTitle = roles.isTellerAdmin ? 'Teller Admin' : 'Teller';
    }

    if(roles.isAgent) {
      jobTitle = roles.isAgentAdmin ? 'Agent Admin' : 'Agent';
    }

    return (
      <header className="main-header">
        <Link to="/" className="logo">
          <span className="logo-mini">
            <b>THE</b>MPB
          </span>
          <span className="logo-lg">
            <b>THE</b> | MPB
          </span>
        </Link>

        <nav className="navbar navbar-static-top" role="navigation">
          <a
            href="#"
            className="sidebar-toggle"
            data-toggle="offcanvas"
            role="button"
          >
            <span className="sr-only">Toggle navigation</span>
          </a>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li className="dropdown notifications-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-bell-o" />
                  <span className="label label-warning" />
                </a>
                <ul className="dropdown-menu">
                  <li className="header">You have 0 notifications</li>
                  <li>
                    <ul className="menu">
                      <li>
                        {/*
                          <a href="#">
                            <i className="fa fa-users text-aqua"></i> 5 new members joined today
                          </a>
                        */}
                        <a href="#">You have no new messages</a>
                      </li>
                    </ul>
                  </li>
                  <li className="footer">
                    <a href="#">View all</a>
                  </li>
                </ul>
              </li>
              <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <img
                    src="/img/user.jpg"
                    width="160"
                    height="160"
                    className="user-image"
                    alt="User Image"
                  />
                  <span className="hidden-xs">
                    {user && user.profile.firstName}
                  </span>
                </a>
                <ul className="dropdown-menu">
                  <li className="user-header">
                    <img
                      src="/img/user.jpg"
                      className="img-circle"
                      alt="User Image"
                    />
                      <p>
                        {user && user.profile.firstName}{" "}
                        {user && user.profile.lastName}
                        {" "} <br></br>
                        <small>
                          {user && user.department}
                        </small>
                      </p>
                  </li>
                  <li className="user-footer">
                    <div className="pull-left">
                      <Link
                        to={`/user/userprofile/${this.state.user}`}
                        className="btn btn-default btn-flat"
                      >
                        Profile
                      </Link>
                    </div>
                    <div className="pull-right">
                      <button
                        onClick={this.logout}
                        className="btn btn-default btn-flat"
                      >
                        Sign out
                      </button>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

MainHeader.contextTypes = {
  router: React.PropTypes.object,
};
