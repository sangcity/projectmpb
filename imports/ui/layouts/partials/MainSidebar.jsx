
import React, { Component } from 'react';

import activeComponent from 'react-router-active-component';

export default class MainSidebar extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const NavItem = activeComponent('li');

    const { roles } = this.props;

    return (
      <aside className="main-sidebar">
        <section className="sidebar">
          <div className="logo" style={{ marginRight: "2px" }}>
            <img
              width="230px"
              src="/img/MPB-Logo.jpg"
              alt="LOGO"
            />
          </div>
          {roles.admin ? (
            <div>
              <ul className="sidebar-menu">
                <NavItem to="/" onlyActiveOnIndex>
                  <i className="fa fa-dashboard" />
                  <span>Dashboard</span>
                </NavItem>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-building" />
                    <span>Departments</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <NavItem to="/">
                      <i className="fa fa-handshake-o" /> <span>Biologist</span>
                    </NavItem>
                    <NavItem to="/">
                      <i className="fa fa-calendar" />
                      <span>Genomics</span>
                    </NavItem>
                    <NavItem to="/">
                      <i className="fa fa-map" />
                      <span>BioInformatics</span>
                    </NavItem>
                  </ul>
                </li>
                <NavItem to="/users">
                  <i className="fa fa-users" />
                  <span>Users</span>
                </NavItem>
              </ul>
            </div>
          ) : null}

          {roles.isBiologist ? (
            <div>
              <ul className="sidebar-menu">
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-certificate" /> <span>Projects</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <NavItem to="/">
                      <i className="fa fa-folder" /> <span>Your Projects</span>
                    </NavItem>
                    <NavItem to="/">
                      <i className="fa fa-folder-open" />
                      <span>Create New Project</span>
                    </NavItem>
                  </ul>
                </li>
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-flask" />
                    <span>Samples</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <NavItem to="/samples">
                      <i className="fa fa-plus" />
                      <span>New Samples</span>
                    </NavItem>
                  </ul>
                </li>
              </ul>
            </div>
          ) : null}

          {roles.isGenomics ? (
            <div>
              <ul className="sidebar-menu">
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-certificate" /> <span>Projects</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <NavItem to="/">
                      <i className="fa fa-kuser-cog" />{" "}
                      <span>Your Projects</span>
                    </NavItem>
                    <NavItem to="/">
                      <i className="fa fa-folder-plus" />
                      <span>Create New Project</span>
                    </NavItem>
                  </ul>
                </li>
                <NavItem to="/samples">
                  <i className="fa fa-users" />
                  <span>Samples</span>
                </NavItem>
              </ul>
            </div>
          ) : null}

          {roles.isBioInformatics ? (
            <div>
              <ul className="sidebar-menu">
                <li className="treeview">
                  <a href="#">
                    <i className="fa fa-certificate" /> <span>Projects</span>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-left pull-right" />
                    </span>
                  </a>
                  <ul className="treeview-menu">
                    <NavItem to="/">
                      <i className="fa fa-kuser-cog" />{" "}
                      <span>Your Projects</span>
                    </NavItem>
                    <NavItem to="/">
                      <i className="fa fa-folder-plus" />
                      <span>Create New Project</span>
                    </NavItem>
                  </ul>
                </li>
                <NavItem to="/samples">
                  <i className="fa fa-users" />
                  <span>Samples</span>
                </NavItem>
              </ul>
            </div>
          ) : null}
        </section>
      </aside>
    );
  }
}
