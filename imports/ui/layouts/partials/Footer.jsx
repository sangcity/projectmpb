
import React, { Component } from 'react';

export default class Footer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right hidden-xs">
          <strong>Powered with <i className="fa fa-heart"></i> & <i className="fa fa-coffee"></i> by <a target="_blank" href="#">SANG</a>.</strong>
        </div>
        <strong>Copyright &copy; 2020.</strong> All rights reserved.
      </footer>
    )
  }
}
