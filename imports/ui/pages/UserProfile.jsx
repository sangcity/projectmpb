import React, { Component } from 'react';
import { Link } from 'react-router';

// import StatPanel from '../components/stat-panel/StatPanel.jsx';
// import MembersTable from '../components/members/MembersTable.jsx';

import { initLayout } from '../../startup/client/lib/init-layout.js';

export default class UserProfilePage extends Component {
  constructor(props) {
    super(props);
  }
  
  componentDidMount() {
    initLayout();
    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);
  }
  
  render() {
    
    const { user, loading, roles } = this.props;
    console.log(user.dateEmployed);
    console.log('User', user);
    
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            User profile
            <small> </small>
          </h1>
          <ol className="breadcrumb">
            <li>
              <Link to="/">
                <i className="fa fa-dashboard" />
                Dashboard
              </Link>
            </li>
            <li>users</li>
            <li className="active">profile</li>
          </ol>
        </section>

        <section className="content">
          {loading ? (
            <div>Loading...</div>
          ) : (
            <section
              className="card box-body box-profile col-md-6 pull-center"
              key={this.props} 
              
            >
              <div className="pull-right">
                <div className="card-header user-header alt bg-dark">
                  <div className="media">
                    <a href="#">
                      <img
                        className="profile-user-img img-responsive img-circle mt-10"
                        src="/img/user.jpg"
                        alt="User profile picture"
                      />
                    </a>
                    <div className="media-body">
                      <h2 className="text-light display-6">
                        {user.profile.firstName} {user.profile.lastName}
                      </h2>
                      <p>{user.jobTitle}</p>
                    </div>
                  </div>
                </div>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                    <i className="fa fa-envelope-o" /> Email{" "}
                    <span className="badge badge-primary pull-right">
                      {user.emails[0].address}
                    </span>
                  </li>
                  <li className="list-group-item">
                    <i className="fa fa-building" /> Department{" "}
                    <span className="badge badge-danger pull-right profile-dept">
                      {user.department}
                    </span>
                  </li>
                  <li className="list-group-item">
                    <i className="fa fa-mobile" /> Mobile{" "}
                    <span className="badge badge-success pull-right">
                      {user.mobile}
                    </span>
                  </li>
                </ul>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                    <i className="fa fa-mobile" /> Active Status{" "}
                  </li>
                  <li className="list-group-item">
                    <p>
                      <span className="label label-danger">{user.isEnabled}</span>
                      {/* <span className="label label-success">Coding</span>
                      <span className="label label-info">Javascript</span>
                      <span className="label label-warning">PHP</span>
                      <span className="label label-primary">Node.js</span> */}
                    </p>
                  </li>
                </ul>
              </div>
            </section>
          )}
        </section>
        {/* <section className="content mt-50">
          {loading ? (
            <div>Loading...</div>
          ) : (
            <div className="col-md-6">
              <div className="box box-primary">
                <div className="box-body box-profile">
                  <img
                    className="profile-user-img img-responsive img-circle"
                    src="/img/user.jpg"
                    alt="User profile picture"
                  />

                  <h3 className="profile-username text-center">
                    {user && user.profile.firstName}
                  </h3>

                  <p className="text-muted text-center">
                    {user && user.jobTitle}
                  </p>

                  <ul className="list-group list-group-unbordered">
                    <li className="list-group-item">
                      <b>Department</b>{" "}
                      <a className="pull-right">{user && user.department}</a>
                    </li>
                    <li className="list-group-item">
                      <b>Mobile Number</b>{" "}
                      <a className="pull-right">{user && user.mobile}</a>
                    </li>
                    <li className="list-group-item">
                      <b>Email Address</b> <a className="pull-right">{ user && user.emails}</a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="box box-primary">
                <div className="box-header with-border">
                  <h3 className="box-title">About Me</h3>
                </div>
                <div className="box-body">
                  <strong>
                    <i className="fa fa-pencil margin-r-5" /> Roles
                  </strong>

                  <p>
                    <span className="label label-danger">UI Design</span>
                    <span className="label label-success">Coding</span>
                    <span className="label label-info">Javascript</span>
                    <span className="label label-warning">PHP</span>
                    <span className="label label-primary">Node.js</span>
                  </p>

                  <hr />

                  <strong>
                    <i className="fa fa-file-text-o margin-r-5" /> Notes
                  </strong>

                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Etiam fermentum enim neque.
                  </p>
                </div>
              </div>
            </div>
          )}
        </section> */}
      </div>
    );
  }
}

