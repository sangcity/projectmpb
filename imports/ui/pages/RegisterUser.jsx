
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import React, { Component } from 'react';

import parsley from 'meteor/amr:parsley.js';
import select2 from 'meteor/natestrauser:select2';
import { enrollUser } from '../../api/users/methods.js';
import LoadingButton from '../components/LoadingButton.jsx';
import '/imports/startup/client/plugins/inputmask/jquery.inputmask.js';
import { initLayout } from '/imports/startup/client/lib/init-layout.js';

export default class RegisterUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formSubmitting: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    initLayout();
    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    $('.select2').select2();
    $('[data-mask]').inputmask();

    this.form = $('#form').parsley({ trigger: 'change' });
    ReactDOM.findDOMNode(this.refs.firstName).focus();
  }

  onSubmit(event) {
    event.preventDefault();

    if (!this.form.isValid()) {
      return;
    }

    this.setState({
      formSubmitting: true,
    });

    let roles;
    let rolesSelect = ReactDOM.findDOMNode(this.refs.roles);
      roles = [].filter.call(rolesSelect.options, function (o) {
      return o.selected;
    }).map(function (o) {
      return o.value;
    });

    const data = {
      roles,
      email: ReactDOM.findDOMNode(this.refs.email).value.trim(),
      mobile: ReactDOM.findDOMNode(this.refs.mobile).value.trim(),
      lastName: ReactDOM.findDOMNode(this.refs.lastName).value.trim(),
      firstName: ReactDOM.findDOMNode(this.refs.firstName).value.trim(),
      department: ReactDOM.findDOMNode(this.refs.department).value.trim(),
    };

    enrollUser.call(data, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o');
        this.setState({
          formSubmitting: false,
        });
        console.log(this.props.firstName);
      }

      if (result) {
        Bert.alert({
          type: 'success',
          title: 'Success',
          icon: 'fa-check',
          style: 'growl-top-right',
          message: 'User registered succesfully',
        });

        this.setState({
          formSubmitting: false,
        });

        this.context.router.replace('/users');
      }
    });
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>User registration</h1>
          <ol className="breadcrumb">
            <li>
              <Link href="/users">
                <i className="fa fa-users" /> Users
              </Link>
            </li>
            <li className="active">create new</li>
          </ol>
        </section>
        <section className="content">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-xs-12">
              <form onSubmit={this.onSubmit} id="form" ref="form">
                <div className="box box-success">
                  <div className="box-header with-border">
                    <h3 className="box-title">Create new user</h3>
                    <div className="box-tools pull-right">
                      <button
                        type="button"
                        className="btn btn-box-tool"
                        data-widget="collapse"
                      >
                        <i className="fa fa-minus" />
                      </button>
                    </div>
                  </div>
                  <div className="box-body">
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="firstName">First name</label>
                          <span className="error"> *</span>
                          <input
                            type="text"
                            ref="firstName"
                            className="form-control"
                            placeholder="Eg. Karim"
                            data-parsley-required="true"
                            defaultValue={this.props.firstName}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="lastName">Last name</label>
                          <span className="error"> *</span>
                          <input
                            type="text"
                            ref="lastName"
                            className="form-control"
                            placeholder="Eg. Maneé"
                            data-parsley-required="true"
                            defaultValue={this.props.lastName}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="email">Email</label>
                          <span className="error"> *</span>
                          <input
                            type="email"
                            ref="email"
                            className="form-control"
                            data-parsley-type="email"
                            placeholder="info@mrc.gm"
                            data-parsley-required="true"
                            defaultValue={this.props.email}
                          />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="mobile">Mobile:</label>
                          <span className="error"> *</span>
                          <div className="input-group">
                            <div className="input-group-addon">
                              <i className="fa fa-phone" />
                            </div>
                            <input
                              data-mask
                              type="text"
                              ref="mobile"
                              className="form-control"
                              data-parsley-minlength="6"
                              data-parsley-required="true"
                              data-inputmask='"mask": "9999999"'
                              defaultValue={this.props.mobile}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="job-title">Job title:</label>
                          <span className="error"> *</span>
                          <input
                            type="text"
                            ref="department"
                            className="form-control"
                            data-parsley-required="true"
                            defaultValue={this.props.jobTitle}
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="roles">Roles</label>
                          <span className="error"> *</span>
                          <select
                            ref="roles"
                            multiple="multiple"
                            style={{ width: "100%" }}
                            data-parsley-required="true"
                            className="form-control select2"
                            data-placeholder="Select what this user can do"
                            defaultValue={this.props.roles}
                          >
                            <option value="" />
                            <option value="biologist">Biologist</option>
                            <option value="bioinformatics">BioInformatics</option>
                            <option value="genomics">Genomics</option>
                            <option value="super-admin">Administrator</option>
                            <option value="create-user">Create User</option>
                            <option value="can-read">Read</option>
                            <option value="can-analyse">Analyse</option>
                            <option value="can-write">Write</option>
                            <option value="can-generate-report">
                              Generate Report
                            </option>
                            <option value="can-view">View</option>
                            <option value="can-submit-report">
                              Submit Report
                            </option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="box-footer">
                    <Link to="/users" className="btn btn-primary btn-flat">
                      <i className="fa fa-close" /> Cancel
                    </Link>
                    <LoadingButton
                      label="Send Invitation"
                      iconClassName="fa fa-user-plus"
                      loading={this.state.formSubmitting}
                      className="btn btn-primary btn-flat pull-right"
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

RegisterUser.propTypes = {
  roles: React.PropTypes.object,
};

RegisterUser.contextTypes = {
  router: React.PropTypes.object.isRequired,
};
