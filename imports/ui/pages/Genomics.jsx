import { Link } from "react-router";
import React, { Component } from "react";

import { initLayout } from "../../startup/client/lib/init-layout.js";

export default class Genomics extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    initLayout();
    setTimeout(function() {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);
  }

  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <ol className="breadcrumb">
            <li className="active">
              <i className="fa fa-dashboard" />
              Home
            </li>
          </ol>
        </section>

        <section className="content mt-50">
          <h1>Genomics</h1>
          <div className="row" />
          <br />
          <div className="row">
            <div className="col-md-12">
              <p>hello to the Genomics team</p>
              {/* <ClientsTable /> */}
            </div>
          </div>
        </section>
      </div>
    );
  }
}
Genomics.propTypes = {
  // totalClients: React.PropTypes.number.isRequired,
  // totalAllocation: React.PropTypes.number.isRequired,
  // totalApplications: React.PropTypes.number.isRequired,
};
