import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Bert } from 'meteor/themeteorchef:bert';

import parsley from 'meteor/amr:parsley.js';
import LoadingButton from '../components/LoadingButton.jsx';
import { usingDefaultPassword } from '../../api/users/methods.js';
import { initLayout } from '../../startup/client/lib/init-layout.js';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formSubmitting: false,
    };

    this.form;
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    initLayout();

    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    this.form = $('form').parsley({ trigger: 'change' });
    ReactDOM.findDOMNode(this.refs.email).focus();
  }

  onSubmit(event) {
    event.preventDefault();

    if (!this.form.isValid()) {
      return;
    }

    this.setState({
      formSubmitting: true,
    });

    const email = this.refs.email.value;
    const password = this.refs.password.value;

    Meteor.loginWithPassword(email, password, error => {
      if (error) {
        Bert.alert({
          type: 'danger',
          style: 'fixed-top',
          icon: 'fa-frown-o',
          hideDelay: 20000,
          message: 'Username or password incorrect',
        });

        this.setState({
          formSubmitting: false,
        });

        ReactDOM.findDOMNode(this.refs.password).value = '';
        $('#form').parsley().reset();
      } else {
        usingDefaultPassword.call({}, (error, result) => {

          if (error) {
            Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o');
          }

          if (result) {
            Session.set('usingDefaultPassword', result.usingDefaultPassword);

            Bert.alert({
              type: 'success',
              title: 'Success',
              icon: 'fa-check',
              style: 'growl-top-right',
              message: 'Login success',
            });

            this.context.router.push('/');
          }
        });
      }
    });
  }

  render() {
    return (
      <div className="login-box">
        <div className="login-logo">
          <a href="#"><b>THE</b> MPB</a>
        </div>
        <div className="login-box-body">
          <p className="login-box-msg">Sign in to start your session</p>

          <form onSubmit={this.onSubmit} id="form">
            <div className="form-group has-feedback">
              <input
                type="email"
                ref="email"
                data-parsley-type="email"
                data-parsley-required="true"
                className="form-control"
                placeholder="Email"
              />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input
                ref="password"
                type="password"
                placeholder="Password"
                className="form-control"
                data-parsley-required="true"
              />
              <span className="glyphicon glyphicon-lock form-control-feedback" />
            </div>
            <div className="row">
              <div className="col-xs-8">
                <div className="checkbox icheck">
                  <label htmlFor="checkbox" />
                </div>
              </div>
              <div className="col-xs-4">
                <LoadingButton
                  label="Sign In"
                  loading={this.state.formSubmitting}
                  className="btn btn-primary btn-flat btn-block"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Login.contextTypes = {
  router: React.PropTypes.object,
};
