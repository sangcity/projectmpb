
import ReactDOM from 'react-dom';
import React, { Component } from 'react';

import moment from 'moment';
import select2 from 'meteor/natestrauser:select2';
import { Link } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { initLayout } from '../../startup/client/lib/init-layout.js';
import { updateSettings } from '../../api/settings/methods.js';

export default class SettingsPage extends Component {
  constructor(props) {
    super(props);

  let form;
  this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    initLayout();
    setTimeout(() => {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    this.form = $('#form').parsley({ trigger: 'change' });
      ReactDOM.findDOMNode(this.refs.env).focus();
  }

  onSubmit(event){
    event.preventDefault();

      const data = {
        settingId: this.props.settings._id,
        env: ReactDOM.findDOMNode(this.refs.env).value.trim()
      };

      updateSettings.call({ data: data }, (error, result) => {
      if(error) {
        Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o' );
      }

      if(result) {
        Bert.alert({
          type: 'success',
          title: 'Success',
          icon: 'fa-check',
          style: 'growl-top-right',
          message: 'Environment Switched',
        });
      }
    });
  }

  render() {
    const { roles, settings } = this.props;
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
           Settings
          </h1>
          <ol className="breadcrumb">
            <li><a href="/"><i className="fa fa-Settings" />Dashboard</a></li>
            <li className="active">settings</li>
          </ol>
        </section>
        <section className="content">
          <form onSubmit={this.onSubmit} ref="form" id="form" >
              <div className="box box-success">
                <div className="box-header with-border">
                  <h3 className="box-title">Set Environment</h3>
                  <div className="box-tools pull-right">
                    <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus" /></button>
                  </div>
                </div>
                <div className="box-body">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="hostName">Select Environment: <span className="error"> *</span></label>
                            <select
                            ref="env"
                            style={{width: '100%'}}
                            data-parsley-required="true"
                            className="form-control select2"
                            data-placeholder="Select what this user can do"
                            defaultValue="staging">
                            <option value=""></option>
                            <option value="staging">Staging</option>
                            <option value="production">Production</option>
                          </select>
                          </div>
                        </div>
                     
                      </div>
                    </div>
                  </div>
                    <div className="box-footer">
                    <button type="subumit" className="btn btn-success btn-flat pull-left">Save</button>
              </div>
                </div>
              </div>
            </form>
        </section>
      </div>
    );
  }
}

SettingsPage.propTypes = {
};
