import { Link } from 'react-router';
import React, { Component } from 'react';

import { initLayout } from '../../startup/client/lib/init-layout.js';
import { Roles } from "meteor/alanning:roles";


import ReactHighcharts from 'react-highcharts';
import DashboardStatPanel from '../components/stat-panel/DashboardStatPanel.jsx';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

   componentWillUpdate() {

  }

  componentDidMount() {
    initLayout();
    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);
  }

  render() {
    const { stats } = this.props.data;

    let totalUsers = 0;
    let totalClients = 0;
    let totalAllocation = 0;
    let totalLandApplications = 0;

    if (stats && stats.length) {
      totalUsers = stats[0].totalUsers;
      totalClients = stats[0].totalClients;
      totalAllocation = stats[0].totalAllocation;
      totalLandApplications = stats[0].totalLandApplications;
    }

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>MPB</h1>
          <ol className="breadcrumb">
            <li className="active">
              <i className="fa fa-dashboard" />Dashboard
            </li>
          </ol>
        </section>

        <section className="content">
          <DashboardStatPanel
            totalUsers={totalUsers}
            totalClients={totalClients}
            totalAllocation={totalAllocation}
            totalLandApplications={totalLandApplications}
          />
          <div className="row">
            <div className="col-md-8 flowChart">
              <ReactHighcharts
                config={{
                  chart: {
                    type: "column"
                  },

                  title: {
                    text: "Weekly Team Summary Report"
                  },

                  xAxis: {
                    categories: [
                      "Acceptance",
                      "Strategy & Planning",
                      "Financial Statements - Check of Balance",
                      "Review",
                      "Finalization"
                    ]
                  },

                  yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                      text: "Number of Audits"
                    }
                  },

                  tooltip: {
                    formatter: function() {
                      return (
                        "<b>" +
                        this.x +
                        "</b><br/>" +
                        this.series.name +
                        ": " +
                        this.y +
                        "<br/>" +
                        "Total: " +
                        this.point.stackTotal
                      );
                    }
                  },

                  colorAxis: {
                    min: 0,
                    max: 30
                  },

                  series: [
                    {
                      colorKey: "colorValue",
                      name: "Acceptance",
                      data: [5],
                      stack: "male"
                    },
                    {
                      name: "Strategy",
                      data: [3, 4],
                      stack: "male",
                      colorValue: 100
                    },
                    {
                      name: "Financial Statements",
                      data: [2, 5, 6, 2, 1],
                      stack: "female"
                    },
                    {},
                    {
                      name: "Review",
                      data: [7, 3, 7, 2, 1],
                      stack: "female"
                    },
                    {
                      name: "Finalization",
                      data: [3, 0, 4, 4, 3],
                      stack: "female"
                    }
                  ]
                }}
              />
            </div>
            <div className="col-md-4">
              <ReactHighcharts
                config={{
                  chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: "pie"
                  },
                  title: {
                    text: "Laboratory Test Types"
                  },
                  tooltip: {
                    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
                  },
                  plotOptions: {
                    pie: {
                      allowPointSelect: true,
                      cursor: "pointer",
                      dataLabels: {
                        enabled: false
                      },
                      showInLegend: true
                    }
                  },
                  series: [
                    {
                      name: "Brands",
                      colorByPoint: true,
                      data: [
                        {
                          name: "Acceptance",
                          y: 56.33
                        },
                        {
                          name: "Strategy & Planning",
                          y: 24.03,
                          sliced: true,
                          selected: true
                        },
                        {
                          name: "Financial Statements - Check of Balances",
                          y: 10.38
                        },
                        {},
                        {
                          name: "Review",
                          y: 16.38
                        },
                        {
                          name: "Finalization",
                          y: 4.77
                        }
                      ]
                    }
                  ]
                }}
              />
            </div>
            <div className="col-md-12 networkgraph mt-30">
              <ReactHighcharts
                config={{
                  series: {
                    type: "networkgraph"
                  },

                  title: {
                    text: "Weekly Team Summary Report"
                  },

                  xAxis: {
                    categories: [
                      "Acceptance",
                      "Strategy & Planning",
                      "Financial Statements - Check of Balance",
                      "Review",
                      "Finalization"
                    ]
                  },

                  yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                      text: "Number of Audits"
                    }
                  },

                  tooltip: {
                    formatter: function() {
                      return (
                        "<b>" +
                        this.x +
                        "</b><br/>" +
                        this.series.name +
                        ": " +
                        this.y +
                        "<br/>" +
                        "Total: " +
                        this.point.stackTotal
                      );
                    }
                  },

                  colorAxis: {
                    min: 0,
                    max: 30
                  },

                  series: [
                    {
                      colorKey: "colorValue",
                      name: "Acceptance",
                      data: [5],
                      stack: "male"
                    },
                    {
                      name: "Strategy",
                      data: [3, 4],
                      stack: "male",
                      colorValue: 100
                    },
                    {
                      name: "Financial Statements",
                      data: [2, 5, 6, 2, 1],
                      stack: "female"
                    },
                    {},
                    {
                      name: "Review",
                      data: [7, 3, 7, 2, 1],
                      stack: "female"
                    },
                    {
                      name: "Finalization",
                      data: [3, 0, 4, 4, 3],
                      stack: "female"
                    }
                  ]
                }}
              />
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col-md-12">
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Dashboard.propTypes = {
  // totalClients: React.PropTypes.number.isRequired,
  // totalAllocation: React.PropTypes.number.isRequired,
  // totalApplications: React.PropTypes.number.isRequired,
};
