import { Link, browserHistory } from 'react-router';
import React, { Component } from 'react';

import UsersTable from '../components/users/UsersTable.jsx';
import { initLayout } from '../../startup/client/lib/init-layout.js';

export default class UsersPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    initLayout();
    setTimeout(() => {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    const { roles } = this.props;
    if (!roles.canCreateUser) {
      Bert.alert({
        type: 'danger',
        title: 'Error',
        icon: 'fa-frown-o',
        style: 'growl-top-right',
        message: 'Unauthorized operation',
      });
      browserHistory.replace('/');
    }
  }

  render() {
    const { roles, users, loading } = this.props;

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            <Link to="user/register" className="btn btn-success btn-flat">
              <i className="fa fa-plus"> New user</i>
            </Link>
          </h1>

          <ol className="breadcrumb">
            <li><a href="#"><i className="fa fa-dashboard" /> /</a></li>
            <li className="active">Users</li>
          </ol>
        </section>

        <section className="content">
          <UsersTable users={users} />
        </section>
      </div>
    );
  }
}
