import React from 'react';
import { Link } from 'react-router';

const LandingPage = () => (
  <div className="lockscreen-wrapper">
    <div className="lockscreen-logo">
      <a href="../../index2.html"><b>THE</b>MPB</a>
    </div>

    <div className="lockscreen-name text-center"><b>The New Platform Where Malaria is Inferior</b></div>

    <div className="text-center mt-50">
      <Link to="/login" className="btn btn-primary btn-flat mr-20">Biologist</Link>
      <Link to="/login" className="btn btn-primary btn-flat mr-20">BioInformatics</Link>
      <Link to="/login" className="btn btn-primary btn-flat mr-20">Genomics</Link>
    </div>
  </div>
);

export default LandingPage;