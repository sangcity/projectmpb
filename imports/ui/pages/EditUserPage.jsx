
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { PropTypes } from 'prop-types';
import React, { Component } from 'react';

import { initLayout } from '/imports/startup/client/lib/init-layout.js';

import EditUser from '../components/users/partials/EditUser.jsx';

export default class EditUserPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    initLayout();
    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);
  }

  render() {
    const { user, loading } = this.props;
    console.log('User', user)

    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>Edit User</h1>
          <ol className="breadcrumb">
            <li><a href="#"><i className="fa fa-dashboard"></i> Users</a></li>
            <li className="active">edit</li>
          </ol>
        </section>

        <section className="content">
          {loading ? (
            <div>Loading...</div>
          ) : (
            <EditUser user={user} loading={loading} />
          )}
        </section>
      </div>
    );
  }
}

EditUserPage.propTypes = {
  user: PropTypes.object.isRequired
}
