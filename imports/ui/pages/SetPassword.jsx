
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';

import parsley from 'meteor/amr:parsley.js';
import { setPassword } from '../../api/users/methods.js';
import { initLayout } from '../../startup/client/lib/init-layout.js';

export default class SetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isError: false,
    };

    this.form;
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    if (!Meteor.userId()) {
      this.context.router.push('/login');
    }
  }

  componentDidMount() {
    initLayout();

    setTimeout(function () {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    this.form = $('form').parsley({ trigger: 'change' });
    ReactDOM.findDOMNode(this.refs.password).focus();
  }

  onSubmit(event) {
    event.preventDefault();

    if (!this.form.isValid()) {
      return;
    }

    const password = this.refs.password.value;
    const confirmPassword = this.refs.confirmPassword.value;

    if (password !== confirmPassword) {
      alert('Error! Passwords you entered did not match');
    } else {
      setPassword.call({ password, confirmPassword }, (error, result) => {
        if (error) {
          Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o' );
        }

        if (result) {
          Bert.alert({
            type: 'success',
            title: 'Success',
            icon: 'fa-check',
            style: 'growl-top-right',
            message: 'Password changed succesfully',
          });

          this.context.router.push('/login');
        }
      });
    }
  }

  render() {
    return (
      <div className="login-box">
        <div className="login-logo">
          <a href="#"><b>THE</b> MPB</a>
        </div>

        <div className="login-box-body">
          <p className="login-box-msg"><strong>Please change your password to continue</strong></p>

          <form onSubmit={this.onSubmit} id="form">
            <div className="form-group has-feedback">
              <input
                type="password"
                ref="password"
                className="form-control"
                data-parsley-required="true"
                placeholder="Enter password" />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input
                type="password"
                ref="confirmPassword"
                className="form-control"
                data-parsley-required="true"
                placeholder="Confirm password" />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div className="row">

              <div className="col-xs-12">
                <button type="submit" className="btn btn-primary btn-block btn-flat btn-block">Save</button>
              </div><br /><br />
              <div className="col-xs-12">
                <button type="submit" onClick={this.logout}
                  className="btn btn-default btn-block btn-flat btn-block"> Logout
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

SetPassword.contextTypes = {
  router: React.PropTypes.object,
};
