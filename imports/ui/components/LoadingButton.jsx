
import React from 'react';
import PropTypes from 'prop-types';

const LoadingButton = ({ label, loading, className, tabIndex, iconClassName, style }) => (
  <button
    type="submit"
    style={style}
    disabled={loading}
    className={className}
    tabIndex={`${tabIndex}`}
  >
    { loading ? (
      <i className="fa fa-spinner fa-spin" />
    ) : (
      <i className={iconClassName} />
    )}
    {label}
  </button>
);

LoadingButton.propTypes = {
  style: PropTypes.object,
  tabIndex: PropTypes.string,
  className: PropTypes.string,
  iconClassName: PropTypes.string,
  label: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default LoadingButton;
