import { Link } from 'react-router';
import React, { Component } from 'react';

export default class Status extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const Label = (() => {
      if(this.props.data) {
        return <label style={{borderRadius: '0px'}} className="label label-success">Active</label>;
      } else {
        return <label style={{borderRadius: '0px'}} className="label label-danger">Expired</label>;
      }
    })();
    return (
      <span>{Label}</span>
    )
  }
}
