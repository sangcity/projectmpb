import { Link } from 'react-router';
import React, { Component } from 'react';

export default class ActionBtn extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Link className="btn btn-primary btn-flat btn-sm"
      to={`/cluster/${this.props.data}`}>
        View
      </Link>
    )
  }
}
