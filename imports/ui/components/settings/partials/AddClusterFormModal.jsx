
import ReactDOM from 'react-dom';
import React, { Component } from 'react';

import parsley from 'meteor/amr:parsley.js';
import { insert } from '../../../../api/clusters/methods.js';

export default class AddClusterFormModal extends Component {

  constructor(props) {
    super(props);

    let clusterForm;
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.clusterForm = $('#clusterForm').parsley({ trigger: 'change' });
    ReactDOM.findDOMNode(this.refs.name).focus();
  }

  onSubmit(event) {
    event.preventDefault();

    if (! this.clusterForm.isValid()) {
      return;
    }

    const data = {
      name: ReactDOM.findDOMNode(this.refs.name).value.trim(),
      description: ReactDOM.findDOMNode(this.refs.description).value.trim(),
    };

    insert.call({ data: data }, (error, result) => {
      if(error) {
        console.log(error);
        Bert.alert('Oops Something went wrong!', 'danger', 'growl-top-right', 'fa-frown-o' );
      }

      if(result) {

        Bert.alert({
          type: 'success',
          title: 'Success',
          icon: 'fa-check',
          style: 'growl-top-right',
          message:`${data.name} Added Succesfully`,
        });
        $('#addClusterModal').modal('hide');
        ReactDOM.findDOMNode(this.refs.clusterForm).reset();
      }
    });
  }

  render() {

    return (
      <div id="addClusterModal" className="modal fade">
        <div className="modal-dialog">
          <form onSubmit={this.onSubmit} id="clusterForm" ref="clusterForm">
            <div className="box box-success">
              <div className="box-header with-border">
                <h3 className="box-title">Add Cluster </h3>
              </div>
              <div className="box-body">
                <div className="row">
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Cluster:</label>
                          <input
                            type="text"
                            tabIndex="1"
                            ref="name"
                            className="form-control"
                            data-parsley-required="true"
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label>Description:</label>
                          <input
                            type="text"
                            ref="description"
                            className="form-control"
                            data-parsley-required="true"
                          />
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
              <div className="box-footer">
                <button type="submit" className="btn btn-success btn-flat pull-right" tabIndex="13">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}


AddClusterFormModal.propTypes = {
};

AddClusterFormModal.contextTypes = {
  router: React.PropTypes.object.isRequired,
};
