import { Link } from 'react-router';
import React, { Component } from 'react';

export default class ClientActionBtn extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Link className="btn btn-primary btn-flat btn-sm"
      to={`/client/${this.props.data}`}>
        View
      </Link>
    )
  }
}
