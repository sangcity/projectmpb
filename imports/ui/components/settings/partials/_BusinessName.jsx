import { Link } from 'react-router';
import React, { Component } from 'react';

export default class BusinessName extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const clientId = this.props.rowData._id;

    return (
      <Link to={`/client/${clientId}`}>
        {this.props.data}
      </Link>
    )
  }
}
