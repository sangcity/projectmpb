
import Griddle from 'griddle-react';
import React, { Component } from 'react';

import 'griddle-react-bootstrap/dist/griddle-react-bootstrap.css';
import { BootstrapPager, GriddleBootstrap } from 'griddle-react-bootstrap';
import Status from './partials/_Status.jsx';
import BusinessName from './partials/_BusinessName.jsx';
import ClientActionBtn from './partials/_ClientActionBtn.jsx';

export default class ClusterClientsTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      columnMetaData: [
        {
          'order': 1,
          'visible': true,
          'sortable': false,
          'searchable': false,
          'columnName': '_id',
          'displayName': 'Action',
          'customComponent': ClientActionBtn,
        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'name',
          'displayName': 'Name',
        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'businessName',
          'displayName': 'Business name',
          'customComponent': BusinessName

        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'address',
          'displayName': 'Address',
        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'status',
          'displayName': 'Status',
          'customComponent': Status,
        },
      ]
    };
  }

  render() {

    let data = [];
    const { loading, cluster } = this.props;
    console.log(cluster)
    const clients = loading ? [] : cluster.clients();
    console.log('Clients',clients);

    if(clients && clients.length) {
      clients.map((client) => {
        data.push({
          _id: client._id,
          manager: client.name,
          address: client.address,
          status: client.isLicensed(),
          businessName: client.businessName,
        });
      });
    }

    return (
      <div>
        <GriddleBootstrap
          results={data}
          showFilter={true}
          resultsPerPage={5}
          showSettings={true}
          useCustomPagerComponent={true}
          customPagerComponent={ BootstrapPager }
          columnMetadata={this.state.columnMetaData}
          settingsToggleClassName='btn btn-default btn-flat'
          tableClassName={'table table-bordered table-striped table-hover'}
          columns={['businessName', 'manager', 'address', 'status']} />
      </div>
    )
  }
}

ClusterClientsTable.propTypes = {
  loading: React.PropTypes.bool,
  clients: React.PropTypes.array,
};
