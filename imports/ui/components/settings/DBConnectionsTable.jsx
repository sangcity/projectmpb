
import Griddle from 'griddle-react';
import React, { Component } from 'react'

import 'griddle-react-bootstrap/dist/griddle-react-bootstrap.css';
import { BootstrapPager, GriddleBootstrap } from 'griddle-react-bootstrap';

import ActionBtn from './partials/_ActionBtn.jsx';

export default class ClustersTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      columnMetaData: [
        {
          'order': 1,
          'visible': true,
          'sortable': false,
          'searchable': false,
          'columnName': '_id',
          'displayName': 'Action',
          'customComponent': ActionBtn,
        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'name',
          'displayName': 'Cluster',
        },
        {
          'locked': false,
          'visible': true,
          'columnName': 'description',
          'displayName': 'Description',
        },
      ]
    };

  }

  render() {

    let data = [];
    const { loading, clusters } = this.props;

    if(clusters && clusters.length) {
      clusters.forEach((cluster) => {
        data.push({
          _id: cluster._id,
          name: cluster.name,
          description: cluster.description,
        });
      });
    }

    return (
      <div>
        <GriddleBootstrap
          results={data}
          showFilter={true}
          resultsPerPage={5}
          showSettings={true}
          useCustomPagerComponent={true}
          customPagerComponent={ BootstrapPager }
          settingsToggleClassName='btn btn-default btn-flat'
          columnMetadata={this.state.columnMetaData}
          tableClassName='table table-bordered table-striped'
          columns={[ '_id', 'name', 'description',]}/>
      </div>
    )
  }
}

ClustersTable.propTypes = {
  loading: React.PropTypes.bool,
  clusters: React.PropTypes.array,
};
