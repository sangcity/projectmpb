import Griddle from "griddle-react";
import React, { Component } from "react";


export default class UserProfile extends Component {
  constructor(props) {
    super(props);
  }

  render() {
   const { user, loading } = this.props;


    return (
      loading ? <loading /> :
      <div key={user._id}>
        <p>Name</p> - {user.profile.firstName}
      </div>
    );
  }
}

UserProfile.propTypes = {
  user: React.PropTypes.array.isRequired
};
