import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { Link } from "react-router";


import {
  updateUser,
  resetPassword,
  activateAccount,
  deactivateAccount,
  deleteAccount,
} from '../../../../api/users/methods.js';

import '../../../../startup/client/plugins/datepicker/datepicker3.css';
import '../../../../startup/client/plugins/inputmask/jquery.inputmask.js';
import { initLayout } from '../../../../startup/client/lib/init-layout.js';
import datepicker from '../../../../startup/client/plugins/datepicker/bootstrap-datepicker.js';

export default class EditUser extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.activateAccount = this.activateAccount.bind(this);
    this.deactivateAccount = this.deactivateAccount.bind(this);
    this.deleteAccount = this.deleteAccount.bind(this);

    this.state = {
      passwordResetPushed: false,
      status: this.props.user.status,
    };
    console.log(this.state);
  }
  
  componentDidMount() {
    initLayout();
    setTimeout(() => {
      jQuery.AdminLTE.layout.activate();
      jQuery.AdminLTE.layout.fix();
    }, 100);

    $('.select2').select2();

    $('[data-mask]').inputmask();

    $('.date-picker').datepicker({
      autoclose: true,
      dateFormat: 'dd-mm-yyyy',
      startView: 2,
      endDate: '+0d',
    });

    this.form = $('#form').parsley({ trigger: 'change' });
  }

  onSubmit(event) {
    event.preventDefault();

    if (!this.form.isValid()) return;

    let roles;
    const rolesSelect = ReactDOM.findDOMNode(this.refs.roles);
    roles = [].filter.call(rolesSelect.options, (o) => o.selected).map((o) => o.value);

    const data = {
      roles,
      userId: this.props.user._id,
      mobile: ReactDOM.findDOMNode(this.refs.mobile).value.trim(),
      jobTitle: ReactDOM.findDOMNode(this.refs.jobTitle).value.trim(),
      lastName: ReactDOM.findDOMNode(this.refs.lastName).value.trim(),
      firstName: ReactDOM.findDOMNode(this.refs.firstName).value.trim(),
      department: ReactDOM.findDOMNode(this.refs.department).value.trim(),
    };
    console.log('Data', data);

    updateUser.call(data, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o');
      }

      if (result) {
        Bert.alert({
          type: 'success',
          title: 'Success',
          icon: 'fa-check',
          style: 'growl-top-right',
          message: 'User updated succesfully',
        });

        this.context.router.replace('/users');
      }
    });
  }

  deleteAccount() {
    const Users = this.props.user;
    const message = `Are you sure you want to delete ${Users.profile.firstName} ${Users.profile.lastName}?`;

    if (confirm(message)) {
      deleteAccount.call({
        userId: this.props.user._id
      }, (err, result) => {
        err && alert('This does not seem to work');

        if (result) {
          Bert.alert({
            type: 'success',
            title: 'Success',
            icon: 'fa-check',
            style: 'growl-top-right',
            message: 'User succesfully deleted from the system',
          });
          this.setState({
            deleteAccount: false,
          });
        }
      });
    }
    this.context.router.push('/users');
  }

  activateAccount() {
    const data = {
      userId: this.props.user._id,
    };

    activateAccount.call(data, (error, result) => {
      if (error) {
        console.log(error);
      }
      if (result) {
        console.log(result);
        console.log(this.props.user.status);
      }
    });
  }

  resetPassword() {

    const { user } = this.props;

    if (!user) {
      Bert.alert('No user found!', 'danger', 'growl-top-right', 'fa-frown-o');
    }

    this.setState({
      passwordResetPushed: true,
    });

    resetPassword.call({ userId: user._id }, (error, result) => {
      if (error) {
        Bert.alert(error.reason, 'danger', 'growl-top-right', 'fa-frown-o');
        this.setState({
          passwordResetPushed: false,
        });
      }

      if (result) {
        Bert.alert({
          type: 'success',
          title: 'Success',
          icon: 'fa-check',
          style: 'growl-top-right',
          message: 'Password reset completed succesfully',
        });
        this.setState({
          passwordResetPushed: false,
        });
      }
    });
  }

  deactivateAccount() {
    const data = {
      userId: this.props.user._id,
    };

    deactivateAccount.call(data, (error, result) => {
      if (error) {
        console.log(error);
      }
      if (result) {
        console.log(result);
        console.log(this.props.user.status);
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  render() {
    const { user, loading } = this.props;

    let group;
    let roles;
    if (!loading) {
      group = user && Object.keys(user.roles)[0];
      roles = user && user.roles[group];
    }

    return (
      <form onSubmit={this.onSubmit} id="form" ref="form">
        <div className="box box-success">
          <div className="box-header with-border">
            <h3 className="box-title">Edit user</h3>
          </div>
          <div className="box-body">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="firstName">First name</label>
                  <input
                    type="text"
                    tabIndex="1"
                    ref="firstName"
                    className="form-control"
                    placeholder="Eg. Muhammed"
                    data-parsley-required="true"
                    defaultValue={user.profile.firstName}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    type="email"
                    tabIndex="3"
                    ref="email"
                    className="form-control"
                    data-parsley-type="email"
                    placeholder="info@mrc.gm"
                    data-parsley-required="true"
                    defaultValue={user.emails[0].address}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="jobTitle">Job title:</label>
                  <span className="error"> *</span>
                  <input
                    type="text"
                    tabIndex="6"
                    ref="jobTitle"
                    className="form-control"
                    data-parsley-required="true"
                    defaultValue={user.jobTitle}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="department">Department:</label>
                  <span className="error"> *</span>
                  <select
                    type="text"
                    tabIndex="7"
                    ref="department"
                    className="form-control"
                    data-parsley-required="true"
                    defaultValue={user.department}
                  >
                    <option value="" />
                    <option value="super-admin">Admin</option>
                    <option value="genomics">Genomics</option>
                    <option value="biologist">Biologist</option>
                    <option value="bioinformatics">BioInformatics</option>
                  </select>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="lastName">Last name</label>
                  <input
                    type="text"
                    tabIndex="2"
                    ref="lastName"
                    placeholder="Eg. Sagne"
                    className="form-control"
                    data-parsley-required="true"
                    defaultValue={user.profile.lastName}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="mobile">Mobile:</label>
                  <div className="input-group">
                    <div className="input-group-addon">
                      <i className="fa fa-phone" />
                    </div>
                    <input
                      data-mask
                      type="text"
                      ref="mobile"
                      tabIndex="4"
                      className="form-control"
                      data-parsley-minlength="6"
                      defaultValue={user.mobile}
                      data-parsley-required="true"
                      data-inputmask='"mask": "999-9999"'
                    />
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="roles">Roles</label>
                  <select
                    ref="roles"
                    tabIndex="6"
                    multiple="multiple"
                    style={{ width: "100%" }}
                    data-parsley-required="true"
                    className="form-control select2"
                    data-placeholder="Select what this user can do"
                    defaultValue={roles}
                  >
                    <option value="" />
                    <option value="super-admin">Admin</option>
                    <option value="create-user">Create User</option>
                    <option value="can-read">Read</option>
                    <option value="can-write">Write</option>
                    <option value="can-analyse">Analyse</option>
                    <option value="can-view">View</option>
                    <option value="can-generate-report">Generate Report</option>
                    <option value="can-submit-report">Submit Report</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div className="box-footer">
            <Link to="/users" className="btn btn-primary btn-flat">
              Cancel
            </Link>
            <button
              type="subumit"
              tabIndex="15"
              style={{ marginRight: "5px" }}
              className="btn btn-success btn-flat pull-right"
            >
              Save
            </button>
            <button
              type="button"
              style={{ marginRight: "5px" }}
              onClick={this.resetPassword}
              disabled={this.state.passwordResetPushed}
              className="btn btn-danger btn-flat pull-right"
            >
              {loading ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                <i className="fa fa-edit" />
              )}{" "}
              Reset password
            </button>
            {user.status == "active" ? (
              <button
                type="button"
                ref="deactivateUser"
                onClick={this.deactivateAccount}
                style={{ marginRight: "5px" }}
                className="btn btn-danger btn-flat pull-right"
              >
                Deactive account
              </button>
            ) : (
              <button
                type="button"
                ref="activateAccount"
                onClick={this.activateAccount}
                style={{ marginRight: "5px" }}
                className="btn btn-success btn-flat pull-right"
              >
                Activate Account
              </button>
            )}
            <button
              type="button"
              ref="deactivateAccount"
              onClick={this.deleteAccount}
              style={{ marginRight: "5px" }}
              className="btn btn-danger btn-flat pull-right"
            >
              {loading ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                <i className="fa fa-trash" />
              )}{" "}
              Deactivate Account
            </button>
          </div>
        </div>
      </form>
    );
  }
}

EditUser.contextTypes = {
  router: React.PropTypes.object.isRequired,
};
