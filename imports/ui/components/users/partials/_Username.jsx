import React, { Component } from 'react';

export default class FullName extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <span>{this.props.data[0].address}</span>
    )
  }
}
