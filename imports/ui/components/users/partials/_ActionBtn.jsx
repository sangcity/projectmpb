import { Link } from 'react-router';
import React, { Component } from 'react';

export default class ActionBtn extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Link
        to={'user/edit/' + this.props.data}
        className="btn btn-primary btn-flat btn-sm">
        <i className="fa fa-edit"></i> Edit
      </Link>
    )
  }
}
