
import Griddle from 'griddle-react';
import React, { Component } from 'react';

import FullName from './partials/_FullName.jsx';
import Username from './partials/_Username.jsx';
import ActionBtn from './partials/_ActionBtn.jsx';

export default class UsersTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      columnMetaData: [
        {
          'order': 1,
          "visible": true,
          "sortable": false,
          "searchable": false,
          "columnName": '_id',
          "displayName": 'Action',
          "customComponent": ActionBtn,
        },
      ]
    };
  }

  componentDidMount() {
  }

  render() {

    const { users } = this.props;

    let data = [];
    if(users && users.length) {
      users.forEach((user) => {
        const profile = user.profile;
        data.push({
          _id: user._id,
          Mobile: user.mobile,
          Email: user.emails[0].address,
          FullName: `${profile.firstName} ${profile.lastName}`,
          Department: user.department,
        });
      });
    }

    return (

      <Griddle
        results={data}
        showFilter={true}
        showSettings={true}
        settingsToggleClassName='btn btn-default'
        columnMetadata={this.state.columnMetaData}
        tableClassName="table table-bordered table-striped"
        columns={['_id', 'FullName', 'Email', 'Mobile', 'Department']} />
    )
  }
}

UsersTable.propTypes = {
  users: React.PropTypes.array.isRequired,
};
