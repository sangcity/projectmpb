import React, { Component } from "react";

export default class FullName extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <span>
        {this.props.data.firstName} {this.props.data.lastName}
      </span>
    );
  }
}
