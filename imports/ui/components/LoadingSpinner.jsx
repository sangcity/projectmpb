import React from 'react';

const LoadingSpinner = () => (
  <div
    style={{
      color: '#00a65a',
      fontSize: '40px',
      textAlign: 'center',
    }}
  >
    <i className="fa fa-spinner fa-spin" />
  </div>
);

export default LoadingSpinner;
