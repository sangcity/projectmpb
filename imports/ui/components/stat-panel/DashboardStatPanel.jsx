
import React, { Component } from 'react';
// import resolvers from '../../../../imports/api/apollo-setup/resolvers';

export default class StatPanel extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {
    totalUsers,
    totalClients,
    totalAllocation,
    totalLandApplications
    } = this.props;

        return (
          <div className="row">
            <div className="col-md-3 col-sm-6 col-xs-6">
              <div className="info-box">
                <span id="bg-aqua" className="info-box-icon bg-aqua">
                  <i className="fa fa-folder-o" />
                </span>
                <div className="info-box-content">
                  <span className="info-box-text">Projects</span>
                  <span className="info-box-number">30</span>
                </div>
              </div>
            </div>
            <div className="col-md-3 col-sm-6 col-xs-6">
              <div className="info-box">
                <span className="info-box-icon bg-yellow">
                  <i className="fa fa-certificate" />
                </span>

                <div className="info-box-content">
                  <span className="info-box-text">Samples</span>
                  <span className="info-box-number">40</span>
                </div>
              </div>
            </div>

            <div className="clearfix visible-sm-block" />

            <div className="col-md-3 col-sm-6 col-xs-6">
              <div className="info-box">
                <span className="info-box-icon bg-green">
                  <i className="fa fa-users" />
                </span>

                <div className="info-box-content">
                  <span className="info-box-text">Users</span>
                  <span className="info-box-number">{totalUsers}</span>
                </div>
              </div>
            </div>
            <div className="col-md-3 col-sm-6 col-xs-6">
              <div className="info-box">
                <span className="info-box-icon bg-red">
                  <i className="fa fa-file-text-o" />
                </span>

                <div className="info-box-content">
                  <span className="info-box-text">Processing Types</span>
                  <span className="info-box-number">5</span>
                </div>
              </div>
            </div>
          </div>
        );
  }
}

StatPanel.propTypes = {

};
