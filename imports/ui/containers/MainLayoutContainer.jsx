
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Roles } from 'meteor/alanning:roles';
import { withTracker } from 'meteor/react-meteor-data';

import MainLayout from '../layouts/MainLayout.jsx';

export default withTracker(() => {
  const userHandle = Meteor.subscribe('user', Meteor.userId() || '');

  const loading = !userHandle.ready();
  const loggedIn = Meteor.userId();
  const user = Meteor.users.findOne(Meteor.userId());
  const usingDefaultPassword = Session.get('usingDefaultPassword');

  const roles = {
    admin: Roles.userIsInRole(Meteor.userId(), 'super-admin', Roles.GLOBAL_GROUP),
    canCreateUser: Roles.userIsInRole(Meteor.userId(), 'create-user', Roles.GLOBAL_GROUP),
    isBiologist: Roles.userIsInRole(Meteor.userId(), 'biologist', Roles.GLOBAL_GROUP),
    isBioInformatics: Roles.userIsInRole(Meteor.userId(), 'bioinformatics', Roles.GLOBAL_GROUP),
    isGenomics: Roles.userIsInRole(Meteor.userId(), 'genomics', Roles.GLOBAL_GROUP),
    canRead: Roles.userIsInRole(Meteor.userId(), 'can-read', Roles.GLOBAL_GROUP),
    canAnalyse: Roles.userIsInRole(Meteor.userId(), 'can-analyse', Roles.GLOBAL_GROUP),
    canView: Roles.userIsInRole(Meteor.userId(), 'can-view', Roles.GLOBAL_GROUP),
    canWrite: Roles.userIsInRole(Meteor.userId(), 'can-write', Roles.GLOBAL_GROUP),
    canGenerateReport: Roles.userIsInRole(Meteor.userId(), 'can-generate-report', Roles.GLOBAL_GROUP),
    canSubmitReport: Roles.userIsInRole(Meteor.userId(), 'can-submit-report', Roles.GLOBAL_GROUP),
  };

  Session.set('roles', roles);

  return {
    user,
    roles,
    loading,
    loggedIn,
    usingDefaultPassword,
  };
})(MainLayout);
