import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Session } from 'meteor/session';

import SettingsPage from '../pages/SettingsPage.jsx';
import { Settings } from '../../api/settings/settings.js';

const SettingsContainer = createContainer(() => {
const settingsHandle = Meteor.subscribe('settings.all');
  const roles = Session.get('roles');

const loading = !settingsHandle.ready();

  const settings  = loading ? {} : Settings.findOne();
  return {
    settings,
    loading,
  };
}, SettingsPage);

export default SettingsContainer;

