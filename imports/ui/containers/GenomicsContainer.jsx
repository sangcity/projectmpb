import { Meteor } from "meteor/meteor";
import { Session } from "meteor/session";
import { createContainer } from "meteor/react-meteor-data";

import Genomics from "../pages/Genomics.jsx";

export default (GenomicsContainer = createContainer(() => {
  const userHandle = Meteor.subscribe("samples.all");

  const loading = !userHandle.ready();
  const roles = Session.get("roles");
  const samples = loading ? [] : Meteor.samples.find().fetch();

  return {
    roles,
    samples,
  };
}, Genomics));
