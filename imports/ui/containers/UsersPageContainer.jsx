import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { createContainer } from 'meteor/react-meteor-data';

import UsersPage from '../pages/UsersPage.jsx';

export default UsersPageContainer = createContainer(() => {
  const usersHandle = Meteor.subscribe('users.all');

  const loading = !usersHandle.ready();
  const roles = Session.get('roles');
  const users = loading ? [] : Meteor.users.find().fetch();

  return {
    roles,
    users,
  };
}, UsersPage);
