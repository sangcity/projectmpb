import { Meteor } from "meteor/meteor";
import { Session } from "meteor/session";
import { createContainer } from "meteor/react-meteor-data";

import BioInformatics from "../pages/BioInformatics.jsx";

export default BioInformaticsContainer = createContainer(() => {
  
  const userHandle = Meteor.subscribe("rooms.all");
  
  const loading = !userHandle.ready();
  const roles = Session.get("roles");
  const rooms = loading ? [] : Meteor.rooms.find().fetch();

  return {
    roles,
    rooms,
  };
}, BioInformatics);
