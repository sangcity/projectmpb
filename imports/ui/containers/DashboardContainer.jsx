
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import Dashboard from '../pages/Dashboard.jsx';

const data = gql `
  query getDataForDisplay {
    stats {
      totalUsers
      totalLandAllocation
      totalLicensesIssued
      totalLandApplications
    }
  }
`;


const DashboardContainer = graphql(data, {
  options: (props) => ({
    pollInterval: 5000,
    // endDate: moment().endOf('year'),
    // startDate: moment().startOf('year'),
  }),
})(Dashboard);

export default DashboardContainer;
