// import { Meteor } from "meteor/meteor";
// import { createContainer } from "meteor/react-meteor-data";
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import React, { Component } from "react";

import Biologist from "../pages/Biologist.jsx";

class BiologistsContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let articles = <div></div>
    if (this.props.data.articles && this.props.data.articles instanceof Array) {
      shows = (
        <div>
                    {this.props.data.articles.map(function(article) {
                      return <Biologist key={article.id} show={article}/>
                    })}
                </div>
            )
          }
          
          return articles;
        }
      }


  const articles = gql`
    query articles {
      articles {
        id
        title
        body
      }
    }
  `;

  console.log(articles);

export default BiologistContainer = graphql(articles, {
  options: { pollInterval: 5000 }
})(BiologistsContainer);
// const BiologistContainer = createContainer(() => {
//   const userHandle = Meteor.subscribe("articles.all");

//   const loading = !userHandle.ready();
//   const articles = loading ? [] : Meteor.articles.find({});
//   return {
//     articles,
//     loading,
//   };
// }, Biologist);