import { Meteor } from "meteor/meteor";
import { Session } from "meteor/session";
import { withTracker } from "meteor/react-meteor-data";

import UserProfilePage from "../pages/UserProfile.jsx";

export default UserProfileContainer = withTracker(({ params: { id } }) => {
  const userHandle = Meteor.subscribe('user', id);

  const loading = !userHandle.ready();
  // const roles = Session.get('roles');
  const user = loading ? {} : Meteor.user();
  // const user = loading ? {} : Meteor.users.findOne(id);
  console.log("params", { params: { id } });
  // console.log(this.props.match.params.id);


  return {
    // roles,
    user,
    loading,
  };
})(UserProfilePage);