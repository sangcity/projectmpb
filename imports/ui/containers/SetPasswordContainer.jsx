import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import SetPassword from '../pages/SetPassword.jsx';

const SetPasswordContainer = createContainer(() => {
  const usersHandle = Meteor.subscribe('user', Meteor.userId());

  const loading = !usersHandle.ready();

  const isDefaultPassword = Meteor.users.findOne(Meteor.userId());

  return {
    loading,
    isDefaultPassword,
  };
}, SetPassword);

export default SetPasswordContainer;
