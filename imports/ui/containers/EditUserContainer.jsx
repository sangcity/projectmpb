
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import EditUserPage from '../pages/EditUserPage.jsx';

export default EditUserContainer = withTracker(({ params: { id } }) => {
  const userHandle = Meteor.subscribe('user', id);

  const loading = !userHandle.ready();
  const user = loading ? {} : Meteor.users.findOne(id);


    console.log("params", { params: { id } });

  return {
    user,
    loading,
  };
})(EditUserPage);

