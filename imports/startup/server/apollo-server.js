import { createApolloServer } from 'meteor/apollo';
import { makeExecutableSchema } from 'graphql-tools';

import typeDefs from '/imports/api/apollo-setup/schema';
import resolvers from '/imports/api/apollo-setup/resolvers';


const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

createApolloServer({
  schema,
});