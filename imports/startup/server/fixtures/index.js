
import { Meteor } from 'meteor/meteor';

import faker from 'faker';
import moment from 'moment';

import { seedUsers } from './seedUsers.js';


Meteor.startup(() => {
  // process.env.IGNORE = '--disable-ipc-flooding-protection';
  process.env.MAIL_URL = Meteor.settings.private.MAIL_URL;
  process.env.MONGO_URL = Meteor.settings.MONGO_URL;

  if (Meteor.users.find().count() === 0) {
    seedUsers();
  }
});
