export const seedUsers = () => {
  const users = [
    {
      email: 'fadel@gmail.com',
      password: 'Mfadel2019',
      profile: {
        firstName: 'Muhammed',
        lastName: 'Fadel',
      },
      mobile: '+2203899999',
    },
    {
      email: 'kmane@gmail.com',
      password: 'Kmane2016',
      profile: {
        firstName: 'Karim',
        lastName: 'Mane',
      },
      mobile: '+2203144565',
    },
    {
      email: 'sang@gmail.com',
      password: 'Sang123',
      profile: {
        firstName: 'Sang',
        lastName: 'City',
      },
      mobile: '+2203781964',
    },
  ];

  users.forEach((user) => {
    const newUser = Accounts.createUser(user);
    Roles.addUsersToRoles(
      newUser, [
        'super-admin',
        'create-user',
        'can-read',
        'can-analyse',
        'can-view',
        'can-write',
        'can-generate-report',
        'can-submit-report',
        'biologist',
        'bioinformatics',
        'genomics',
      ], Roles.GLOBAL_GROUP
    );
  });
};
