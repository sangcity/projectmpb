
import 'react-select/dist/react-select.css';
import 'react-bootstrap-daterangepicker/css/daterangepicker.css';
import 'meteor/fortawesome:fontawesome';
import 'meteor/pagebakers:ionicons';
import './stylesheets/bootstrap.min.css';
import './stylesheets/AdminLTE.min.css';
import './stylesheets/skin-blue.min.css';

import './lib/bootstrap.min.js';
import './stylesheets/style.css';
import './stylesheets/certificate.css';
import './config.js';

