
import React from 'react';
import { Router, Route, browserHistory } from 'react-router';

import gql from "graphql-tag";


import ApolloClient from "apollo-boost";
// import {ApolloClient} from "apollo-client";
// import { InMemoryCache } from "apollo-cache-inmemory";
// import { MeteorLink } from "apollo-link-ddp";
import { ApolloProvider } from 'react-apollo';
import { Accounts } from 'meteor/accounts-base';

import Login from '../../ui/pages/Login.jsx';
import SetPassword from '../../ui/pages/SetPassword.jsx';
import RegisterUser from '../../ui/pages/RegisterUser.jsx';
import BiologistContainer from '../../ui/containers/BiologistContainer.jsx';
import BioInformaticsContainer from '../../ui/containers/BioinformaticsContainer.jsx';
import GenomicsContainer from '../../ui/containers/GenomicsContainer.jsx';
import EditUserContainer from '../../ui/containers/EditUserContainer.jsx';
import DashboardContainer from '../../ui/containers/DashboardContainer.jsx';
import UsersPageContainer from '../../ui/containers/UsersPageContainer.jsx';
import MainLayoutContainer from '../../ui/containers/MainLayoutContainer.jsx';
import LandingPage from '../../ui/pages/LandingPage.jsx';
import UserProfileContainer from '../../ui/containers/UserProfileContainer.jsx';
// import UserProfile from '../../ui/pages/UserProfilePage.jsx';
import { onError } from "apollo-link-error";


const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (networkError) {
    // Check if error response is JSON
    try {
      JSON.parse(networkError.bodyText);
    } catch (e) {
      // If not replace parsing error message with real one
      networkError.message = networkError.bodyText;
    }
  }
});

// const customFetch = (uri, options) => {
  //   return fetch(uri, options).then(response => {
    //     if (response.status >= 500) {
      //       // or handle 400 errors
      //       return Promise.reject(response.status);
//     }
//     return response;
//   });
// };

const client = new ApolloClient({
  uri: "http://localhost:3000/graphql",
  fetch: errorLink,
  request: operation =>
  operation.setContext(() => ({
    headers: {
      authorization: Accounts._storedLoginToken()
    }
  }))
});

const test = gql`
  {
    articles {
      id
      title 
      body 
    }
  }
`;

client.query({
  query: test,
}).then(res => console.log(res));


// const client = new ApolloClient({
//   fetch: customFetch,
//   link: new MeteorLink(),
//   cache: new InMemoryCache()
// });


const ApolloApp = () => (
  <ApolloProvider client={client}>
    <Router history={browserHistory}>
      <Route path="/landing" components={LandingPage} />
      <Route path="/login" components={Login} />
      <Route path="/password/set" components={SetPassword} />
      {/* <Route path="/userprofile" components={UserProfile} /> */}


      <Route component={ MainLayoutContainer }>
        <Route path="/" exact components={{ content: DashboardContainer }} />

        <Route path="/biologist" components={{ content: BiologistContainer }} />
        <Route path="/bioinformatics" components={{ content: BioInformaticsContainer }} />
        <Route path="/genomics" components={{ content: GenomicsContainer }} />

        <Route path="/users" components={{ content: UsersPageContainer }} />
        <Route path="/user/register" components={{ content: RegisterUser }} />
        <Route path="/user/userprofile/:id" components={{ content: UserProfileContainer }} />
        <Route path="/user/edit/:id" components={{ content: EditUserContainer }} />
        <Route path="*" components={{ content: DashboardContainer }} />
      </Route>
    </Router>
  </ApolloProvider>
);

// ApolloApp.use(errorHandler);


export default ApolloApp;