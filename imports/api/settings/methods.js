
import { _ } from 'meteor/underscore';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import { Settings } from './settings.js';

// export const insert = new ValidatedMethod({
//   name: 'Settings.insert',
//   validate: new SimpleSchema({
//     data: { type: Object, blackbox: true },
//   }).validator(),
//   run({ data }) {
//     if (!Meteor.userId()) {
//       throw new Meteor.Error(401, 'Unauthorized');
//     }
//     return Settings.insert({
//       ...data,
//       createdBy: this.userId,
//     });
//   },
// });

export const updateSettings = new ValidatedMethod({
  name: 'Settings.update',
  validate: new SimpleSchema({
    data: { type: Object, blackbox: true },
  }).validator(),
  run({ data }) {
    if (!Roles.userIsInRole(Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP)) {
      throw new Meteor.Error(401, 'You are not authorized to conduct this operation!');
    }

    return Settings.update({ _id: data.settingId }, {
      $set: data,
    });
  },
});

export const remove = new ValidatedMethod({
  name: 'Settings.remove',
  validate: new SimpleSchema({
    data: { type: Object },
  }).validator(),
  run({ settingId }) {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'Not logged in');
    }

    const setting = Settings.findOne(settingId);

    if (setting.userId === Meteor.userId()) {
      return Settings.remove({ userId: this.userId });
    }

    throw new Meteor.Error(403, 'You are not authorized to conduct this operation');
  },
});

const LISTS_METHODS = _.pluck([
  remove,
  updateSettings,
], 'name');

if (Meteor.isServer) {
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(LISTS_METHODS, name);
    },

    connectionId() { return true; },
  }, 5, 1000);
}
