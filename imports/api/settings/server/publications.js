

import { Meteor } from 'meteor/meteor';

import { Settings } from '../settings.js';

Meteor.publish('settings.all', function settings() {

  if(! this.userId) {
    return this.ready();
  }

  return Settings.find();
});
Meteor.publish('setting', function setting(id) {

  if(! this.userId) {
    return this.ready();
  }

  return Settings.find({
    _id: id
  }, {
    limit: 1
  });
});
