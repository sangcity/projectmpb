
import { Mongo } from 'meteor/mongo';

class SettingsCollection extends Mongo.Collection {
  insert(doc, callback) {
    return super.insert(doc, callback);
  }
  update(selector, modifier) {
    return super.update(selector, modifier);
  }
  remove(selector) {
    return super.remove(selector);
  }
}

export const Settings = new SettingsCollection('Settings');

Settings.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Settings.schema = new SimpleSchema({

  env: {
    type: String,
    defaultValue: 'staging',
    allowedValues: [
      'staging',
      'production',
    ]
  },

  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset();
      }
    }
  },

  updatedAt: {
    type: Date,
    autoValue: function() {
      if(this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  },

});

Settings.attachSchema(Settings.schema);
