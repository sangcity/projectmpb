
import { Meteor } from 'meteor/meteor';

import {
  Strip,
  Users,
  Clients,
  License,
  Invitations,
  Comments,
  Documents,
  LandOwners,
  LicenseTypes,
  Notifications,
  FollowupNotes,
  LicenseDocuments,
  ClientInspections,
  EnforcementReports,
  LandApplicationDocs,
  LandAllocations,
  LandApplications,
  InspectionSchedules,
  ApplicationDocuments,
  LandInspectionMilestones
} from './connector.js';


import { Articles, Shows } from './connector.js';

export default (resolvers = {
  Query: {
    clients(root, args, context) {
      if (context.userId) {
        const endDate = new Date(args.endDate);
        const startDate = new Date(args.startDate);

        const selectors = {};
        if (args.startDate || args.endDate) {
          selectors.date = { $lte: endDate, $gte: startDate };
        }

        return Clients.find(selectors)
          .sort({
            date: -1
          })
          .toArray();
      }
    },

    // articles(root, args, context){
    //   if(context.userId) {
    //     return Articles("Select * From articles");
    //   }
    // },

    articles(_, args) {
      return Articles.findAll({
        where: args,
      });
    },

    shows(_, args) {
      return Shows.findAll({
        where: args,
      });
    },

    // licenses (root, args, context) {
    //   if (context.userId) {
    //     const endDate = new Date(args.endDate);
    //     const startDate = new Date(args.startDate);

    //     let selectors = {};
    //     if (args.startDate || args.endDate) {
    //       selectors.createdAt = { $lte: endDate, $gte: startDate };
    //     }

    //     return License.find(selectors).toArray();
    //   }
    // },

    // landAllocations(root, args, context) {
    //   if (context.userId) {
    //     const endDate = new Date(args.endDate);
    //     const startDate = new Date(args.startDate);

    //     let selectors = {};
    //     if (args.startDate || args.endDate) {
    //       selectors.createdAt = { $lte: endDate, $gte: startDate };
    //     }

    //     return LandAllocations.find(selectors).toArray();
    //   }
    // },

    // landApplications(root, args, context) {
    //   if (context.userId) {
    //     const endDate = new Date(args.endDate);
    //     const startDate = new Date(args.startDate);

    //     let selectors = {};
    //     if (args.startDate || args.endDate) {
    //       selectors.createdAt = { $lte: endDate, $gte: startDate };
    //     }

    //     return LandApplications.find(selectors).toArray();
    //   }
    // },

    users(root, args, context) {
      if (context.userId) {
        return Users.find().fetch();
      }
    },

    stats(root, args, context) {
      if (context.userId) {
        return [
          {
            totalUsers: Users.find({ userId: { $exists: true } }).count(),
            totalClients: Clients.find().count(),
            totalLandApplications: LandApplications.find().count(),
            totalLandAllocations: LandAllocations.find().count()
          }
        ];
      }
    }
  }

  // Client: {
  //   businessType(client) {
  //     return BusinessTypes.findOne({ _id: client.businessTypeId });
  //   },
  //   createdBy(client) {
  //     return Users.findOne({ _id: client.createdBy });
  //   },
  // },

  // LandAllocation: {
  //   application(allocation) {
  //     return LandApplications.findOne({ _id: allocation.applicationId });
  //   },
  //   createdBy(allocation) {
  //     return Users.findOne({ _id: allocation.createdBy });
  //   },
  // },

  // License: {
  //   createdBy(license) {
  //     return Users.findOne({ _id: trip.createdBy });
  //   },
  // },
});
