import Sequelize from 'sequelize';
import { Meteor } from 'meteor/meteor';

  const MRC_PWD = Meteor.settings.private.MRC_PWD;
  const MRC_HOST = Meteor.settings.private.MRC_HOST;
  const MRC_USERNAME = Meteor.settings.private.MRC_USERNAME;
  const MRC_DBNAME = Meteor.settings.private.MRC_DBNAME;
  const MRC_PORT = Meteor.settings.private.MRC_PORT;


  const MRCDB = new Sequelize(MRC_DBNAME, MRC_USERNAME, MRC_PWD, {
    dialect: 'mysql',
    host: MRC_HOST,
    port: MRC_PORT,
  });

  MRCDB.authenticate().then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(() => {
    console.error('Unable to connect to the database:');
  });

const Articles = MRCDB.define('articles', {
  title: {
    type: Sequelize.STRING,
  },
  body: {
    type: Sequelize.TEXT,
  },
}, {
  timestamps: false,
});


const Shows = MRCDB.define('shows', {
  title: {
    type: Sequelize.STRING,
  },
  body: {
    type: Sequelize.TEXT,
  },
  data: {
    type: Sequelize.DATE,
  }
}, {
  timestamps: false,
});

export { Articles, Shows };

