
const typeDefs = [
  `
    type Strip {
      _id: String
      name: String
      slug: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type Profile {
      firstName: String
      lastName: String
    }

    type LicenseDocument {
      _id: String
      name: String
      label: String
      path: String
      documentId: Document
      uploaded: Boolean
      mandatory: Boolean
      DateUploaded: String
      licenseId: License
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type LicenseType {
      name: String
      slug: String
      requiredDocs: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type License {
      _id: String
      clientId: Client
      remoteClientId: String
      licenseTypeId: LicenseType
      isSynchedWithFinex: Boolean
      cleared: Boolean
      paid: Boolean
      year: String
      transactionDate: String
      amountPaid: Float
      description: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type LandInspectionMilestone {
      _id: String
      description: String
      requirement: String
      monthIndex: Int
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type LandApplicationDocs {
      _id: String
      name: String
      label: String
      path: String
      documentId: Document
      uploaded: Boolean
      datUploaded: Boolean
      lanApplicationId: LandApplication
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type User {
      _id: String
      isEnabled: Boolean
      mobile: String
      profile: Profile
      jobTitle: String
      department: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type LandOwner {
      _id: String
      firstName: String
      lastName: String
      address: String
      phoneNumber: String
      allocationId: LandAllocation
      createdBy: User
      createdAt: String
      updatedAt: String
    }
    
    type ContactPerson {
      firstName: String
      lastName: String
      position: String
      phone: String
    }

    type Dimension {
      length: Float
      breadth: Float
      area: Float
      measuringUnit: String
    }

    type DocumentsCheckList{
      boughtForm: Boolean
      submittedDocs: Boolean
      paidApplication: Boolean
    }

    type LandApplication {
      _id: String
      companyName: String
      address: String
      contactPerson: ContactPerson
      proposedUse: String
      dimensions: Dimension
      projectCost: Float
      projectCostCurrency: String
      numberOfRooms: String
      numberOfBeds: String
      typeOrClass: String
      other: String
      documentsCheckList: DocumentsCheckList
      approved: Boolean
      approvedDate: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type DevelopmentMilestone {
      description: String
      requirement: String
      index: Int
      notes: String
      monthIndex: Int
      inspected: Boolean
      inspectedBy: User
      status: String
      dateScheduled: String
      dateInspected: String
    }

    type LandAllocation {
      _id: String
      ownerId: LandOwner
      application: LandApplication
      dimensions: Dimension
      location: Location
      proposedUse: String
      approvedDate: String
      monthsToDevelop: Int
      developmentMilestones: DevelopmentMilestone
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type FollowupNote {
      _id: String
      title: String
      description: Int
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type PersonInCharge {
      fullName: String
      position: String
      phoneNumber: String
    }

    type Checklist {
      point: Float
      result: String
      number: Float
      category: String
      question: String
      answered: Boolean
    }

    type ClientInspection {
      client: Client
      timestamp: String
      year: String
      personInCharge: PersonInCharge
      grade: String
      score: Float
      remark: String
      checklist: Checklist
      complete: Boolean
      comments: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type EnforcementReport {
      _id: String
      strip: Strip
      type: String
      description: String
      timestamp: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type Document {
      _id: String
    }

    type Comment {
      _id: String
      title: String
      followupNotes: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type ManagerNationality {
      iso_3: String
      countryName: String
    }
    
    type Location {
      cityOrTown: String
      longitude: Float
      latitide: Float
    }

    type Client {
      _id: String
      remoteClientId: String
      name: String
      businessName: String
      description: String
      address: String
      branches: String
      businessType: BusinessType
      numberOfRooms: Int
      numberOfBeds: Int
      touristTransported: String
      strip: Strip
      inspectionStatus: Boolean
      beveragesStatus: Boolean
      apartmentStatus: Boolean
      transportationWorth: Boolean
      insuranceCovers: Boolean
      gamingMachinesStatus: Boolean
      electricalStatus: Boolean
      establishmentStatus: Boolean
      postalAddress: String
      provisionStatus: Boolean
      legalStatus: String
      totalEmployees: Int
      location: Location
      managerNationality: ManagerNationality 
      telephone: String
      email: String
      tourGuideStatus: Boolean
      phoneNumber: String
      approved: Boolean
      approvedDate: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type BusinessType {
      _id: String
      name: String
      slug: String
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type ApplicationDocument {
      _id: String
      name: String
      label: String
      path: String
      document: Document
      uploaded: Boolean
      dateUploaded: String
      landApplication: LandApplication
      createdBy: User
      createdAt: String
      updatedAt: String
    }

    type ClientsReceiptDB {
      Id: String
      BatchNumber: String
      CheequeNo: String
      ClientId: [FinexDB]
      AccountId: String
      Descr: String
      RecDate: String
    }


    type Stat {
      totalUsers: Int
      totalLandApplications: Int
      totalLandAllocation: Int
      totalLicensesIssued: Int
    }

    type Articles {
      id: ID!
      title: String
      body: String
    }

    type Shows {
      id: ID!
      title: String
      body: String
      date: String
    }

    type Query {
      stats: [Stat],
      articles: [Articles],
      shows: [Shows],
      finexClients: [FinexDB],
      clientsReceipts: [ClientsReceiptDB],
      users(userId: String): [User],
      strips(stripId: String): [Strip],
      licenses(startDate: String, endDate: String): [License],
      landInspectionMilestones(countryId: String): [LandInspectionMilestone],
      landApplications(startDate: String, endDate: String): [LandApplication],
      landAllocations(startDate: String, endDate: String): [LandAllocation],
      followUpsNotes(startDate: String, endDate: String, isIssued: Boolean): [FollowupNote],
      enforcementReports(startDate: String, endDate: String): [EnforcementReport]
      documents(startDate: String, endDate: String): [Document]
      comments(startDate: String, endDate: String): [Comment]
      clients(startDate: String, endDate: String): [Client]
      businessTypes(startDate: String, endDate: String): [BusinessType]
    }

    schema {
      query: Query
    }
  `,
];

export default typeDefs;
