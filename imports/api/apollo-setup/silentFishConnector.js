
import mongo from 'then-mongo';

const development = process.env.NODE_ENV === 'development';
const DEV_MONGO_URL = Meteor.settings.DEV_MONGO_URL;
const MONGO_URL = Meteor.settings.MONGO_URL;

const currentMongoUrl = development ? DEV_MONGO_URL : MONGO_URL;

const silentFishDB = mongo(currentMongoUrl);

const FinexClients = silentFishDB.collection('FinexClients');

export { FinexClients };
