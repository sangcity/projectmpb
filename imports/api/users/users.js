//
// import { Mongo } from 'meteor/mongo';
//
// import { membersSchema } from './members-schema.js';
//
// class MembersCollection extends Mongo.Collection {
//   insert(doc, callback) {
//     const result = super.insert(doc, callback);
//     console.log('insert called form memberjs');
//     return result;
//   }
//   update(selector, modifier) {
//     const result = super.update(selector, modifier);
//     console.log('Update called from member js');
//     return result;
//   }
//   remove(selector) {
//     const member = this.find(selector).fetch();
//     const result = super.remove(selector);
//     return result;
//   }
// }
//
// export const Members = new MembersCollection('Members');
//
// // Deny all client-side updates since we will be using methods to manage this collection
// Members.deny({
//   insert() { return true; },
//   update() { return true; },
//   remove() { return true; },
// });
//
// Members.schema = membersSchema;
//
// Members.attachSchema(Members.schema);
