
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

Meteor.publish('users.all', function users() {
  if (!this.userId) {
    return this.ready();
  }

  if (Roles.userIsInRole(this.userId, ['create-user'], Roles.GLOBAL_GROUP)) {
    return Meteor.users.find({}, {
      fields: {
        roles: 1,
        emails: 1,
        mobile: 1,
        profile: 1,
        isEnabled: 1,
        department: 1,
        dateEmployed: 1,
      },
    });
  }

  return Meteor.users.find({ _id: this.userId }, {
    limit: 1,
    fields: {
       profile: 1,
     },
  });
});

Meteor.publish('user', function users(userId) {
  if (!this.userId) {
    return this.ready();
  }

  if (userId === this.userId) {
    return Meteor.users.find({ _id: userId }, {
      fields: {
        roles: 1,
        emails: 1,
        mobile: 1,
        profile: 1,
        jobTitle: 1,
        isEnabled: 1,
        department: 1,
        dateEmployed: 1,
      },
      limit: 1,
    });
  }

  if (Roles.userIsInRole(this.userId, 'create-user', Roles.GLOBAL_GROUP)) {
    return Meteor.users.find({ _id: userId }, {
      fields: {
        roles: 1,
        emails: 1,
        mobile: 1,
        profile: 1,
        jobTitle: 1,
        isEnabled: 1,
        department: 1,
        dateEmployed: 1,
      },
      limit: 1,
    });
  }
});
