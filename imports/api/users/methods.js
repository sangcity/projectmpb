import { Email } from 'meteor/email';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { GetSendUserEmail } from '../../ui/components/users/partials/email-template/EmailTemplate';
// import { faker } from 'meteor/practicalmeteor:faker';
// const faker = null;

export const usingDefaultPassword = new ValidatedMethod({
  name: 'Users.usingDefaultPassword',
  validate: new SimpleSchema({
  }).validator(),
  run() {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'Unauthorized');
    }

    if (Meteor.isServer) {
      const usingDefaultPassword = Meteor.users.findOne(Meteor.userId()).usingDefaultPassword;

      return {
        usingDefaultPassword,
      };
    }
    return null;
  },
});

export const setPassword = new ValidatedMethod({
  name: 'Users.setPassword',
  validate: new SimpleSchema({
    password: { type: String, min: 8 },
    confirmPassword: { type: String, min: 8 },
  }).validator(),
  run({ password, confirmPassword }) {
    if (Meteor.isServer) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(401, 'Not logged in!');
      }

      if (password !== confirmPassword) {
        throw new Meteor.Error(401, 'Error! Passwords did not match. Please try again');
      }

      const user = Meteor.users.findOne(Meteor.userId());

      if (!user.usingDefaultPassword) {
        throw new Meteor.Error(401, 'Default password already changed');
      }

      Meteor.users.update({ _id: user._id }, {
        $set: {
          usingDefaultPassword: false,
        },
      });

      Accounts.setPassword(Meteor.userId(), password);

      return 'success';
    }
    return null;
  },
});

Accounts.config({
  restrictCreationByEmailDomain: 'mrc.gm',
  sendVerificationEmail: true,
});


export const enrollUser = new ValidatedMethod({
  name: 'users.insert',
  validate: new SimpleSchema({
    mobile: { type: String, min: 7 },
    lastName: { type: String, min: 3 },
    firstName: { type: String, min: 3 },
    email: { type: SimpleSchema.RegEx.Email },
    department: {  
      type: String,
      allowedValues: [
        'super-admin',
        'genomics',
        'biologist',
        'bioinformatics',
      ],
    },
    roles: {
      type: [String],
      allowedValues: [
        'super-admin',
        'create-user',
        'biologist',
        'bioinformatics',
        'genomics',
        'can-read',
        'can-analyse',
        'can-view',
        'can-write',
        'can-generate-report',
        'can-submit-report',
      ],
    },
    dateEmployed: { 
      type: Date, optional: true },
  }).validator(),
  run({ roles, mobile, department, lastName, firstName, email }) {
    if (Meteor.isServer) {
      if (!Roles.userIsInRole(Meteor.user(), 'create-user', 'super-admin')) {
        throw new Meteor.Error(403, 'Unauthorized');
      };

      const user = {
        email,
        mobile,
        department,
        profile: {
          lastName,
          firstName,
        },
        password: faker.internet.password(),
        dateEmployed: new Date().toISOString(),
      };    
      // console.log(user.password);

      const newUserId = Accounts.createUser(user);
      console.log('New User',newUserId);
      Roles.addUsersToRoles(newUserId, roles, Roles.GLOBAL_GROUP);
       console.log('password', user.password);
       console.log('date', user.dateEmployed);
       console.log('First Name', user.profile.firstName);

      if (newUserId) {
        this.unblock();
        
        console.log('password', user.password);
        try {
          Meteor.defer(() => {
            const SendUserEmail = `
              <Email title="MPB Email" className="emailTemplate" style={{ fontColor: blue, }}>
                <table>
                  <tbody>
                    <Box>
                      <Item>
                        <p>
                        Hello ${user.profile.firstName} ${user.profile.lastName}, welcome to the Fight Against Malaria as our resourceful ${user.department}. <br><br>
                        Please use the following to get connected, <br> Email: ${user.email} <br> Temporary password: ${user.password}. <br>
                        And confirm by hitting the below mentioned link. <br>
                        <a href="http://localhost:3000/">The MPB Portal</a>
                        </p>
                      </Item>
                    </Box>
                  </tbody>
                </table>
              </Email>
            `;
            Email.send({
              to: email,
              from: 'sanrie10@gmail.com',
              subject: 'Welcome to THE MPB Portal',
              html: SendUserEmail,
            });
          });
        } catch (e) {
          console.log(e);
        }
      }

      return newUserId;
    }
    return null;
  },
});

export const deleteAccount = new ValidatedMethod({
  name: 'users.remove',
  validate: new SimpleSchema({
    userId: { type: String }
  }).validator(),
  run({ userId }) {

    Meteor.users.remove(userId);
  },
});


export const updateUser = new ValidatedMethod({
  name: 'users.update',
  validate: new SimpleSchema({
    userId: { type: String },
    mobile: { type: String, min: 7 },
    lastName: { type: String, min: 3 },
    firstName: { type: String, min: 3 },
    department: {  
      type: String,
      allowedValues: [
        'super-admin',
        'genomics',
        'biologist',
        'bioinformatics',
      ],
    },
    roles: {
      type: [String],
      allowedValues: [
        'super-admin',
        'create-user',
        'can-read',
        'can-analyse',
        'can-view',
        'can-write',
        'can-generate-report',
        'can-submit-report',
      ],
    },
    jobTitle: { type: String },
  }).validator(),
  run({ userId, roles, mobile, department, lastName, firstName, jobTitle }) {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'Unauthorized');
    }

    if (Meteor.isServer) {
      if (Roles.userIsInRole(Meteor.user(), 'create-user', 'super-admin')) {
        const user = Meteor.users.update({ _id: userId }, {
          $set: {
            mobile,
            department,
            jobTitle,
            'profile.firstName': firstName,
            'profile.lastName': lastName,
          },
        });

        Roles.setUserRoles(userId, roles, Roles.GLOBAL_GROUP);

        return user;
      }
    }
  },
});

export const setAccountStatus = new ValidatedMethod({
  name: 'Users.setAccountStatus',
  validate: new SimpleSchema({
    state: { type: Boolean },
    accountId: { type: String },
  }).validator(),
  run({ accountId, state }) {
    if (Meteor.isServer) {
      const user = Meteor.users.findOne(accountId);

      if (user.isEnabled === state) {
        return;
      }

      if (Roles.userIsInRole(Meteor.userId(), ['create-user'], Roles.GLOBAL_GROUP)) {
        if (user._id === Meteor.userId()) {
          throw new Meteor.Error(401, 'Error! You cannot disable your own account.');
        }

        const userUpdated = Meteor.users.update({ _id: accountId }, {
          $set: { isEnabled: state },
        });

        if (userUpdated && state === false) {
          Meteor.users.update(accountId, {
            $set: { 'services.resume.loginTokens': [] },
          });
        }

        return userUpdated;
      }

      if (Roles.userIsInRole(Meteor.userId(), ['admin'], 'agents')) {
        const user = Meteor.users.findOne(accountId);

        if (user.agentId !== Meteor.user().agentId) {
          throw new Meteor.Error(401, 'Unauthorized!');
        }

        if (user._id === Meteor.userId()) {
          throw new Meteor.Error(401, 'Error! You cannot disable your own account.');
        }

        const userUpdated = Meteor.users.update({ _id: accountId }, {
          $set: {
            isEnabled: state,
          },
        });

        if (userUpdated && state === false) {
          Meteor.users.update(accountId, {
            $set: { 'services.resume.loginTokens': [] },
          });
        }

        return userUpdated;
      }

      throw new Meteor.Error(401, 'Unauthorized!');
    }
  },
});

export const resetPassword = new ValidatedMethod({
  name: 'Users.resetPassword',
  validate: new SimpleSchema({
    userId: { type: String },
  }).validator(),
  run({ userId }) {
    if (Meteor.isServer) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(401, 'Not logged in!');
      }

      if (!Roles.userIsInRole(Meteor.user(), 'create-user', 'admin')) {
        throw new Meteor.Error(403, 'Unauthorized');
      }

      const user = Meteor.users.findOne({ _id: userId });

      if (!user) {
        throw new Meteor.Error('user-not-found', 'User not found!');
      }

      // console.log('user', user);
      const password = faker.internet.password();

      console.log('new password', password);

      Accounts.setPassword(user, password);

      Meteor.users.update({ _id: user._id }, {
        $set: {
          usingDefaultPassword: true,
        },
      });

      const email = user.emails[0].address;

      this.unblock();

      try {
        Meteor.defer(() => {
          Email.send({
            to: email,
            from: 'sanrie10@gmail.com',
            subject: 'Password reset',
            html: `Your password has been reset to: ${password}`,
          });
        });
      } catch (e) {
        console.log(e);
      }

      return user;
    }
    return null;
  },
});

if (Meteor.isServer) {
  const currentUser = this.userId;
  const usersCount = Meteor.users.find().count();

  Accounts.onCreateUser((options, user) => {
    user.isEnabled = true;
    user.mobile = options.mobile;
    user.profile = options.profile;
    user.jobTitle = options.jobTitle;
    user.department = options.department;
    user.usingDefaultPassword = true;
    user.department = options.department;
    user.dateEmployed = options.dateEmployed;

    return user;
  });
}
